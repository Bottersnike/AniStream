try:
    from cheroot.wsgi import Server as WSGIServer, PathInfoDispatcher
except ImportError:
    from cherrypy.wsgiserver import CherryPyWSGIServer as WSGIServer, WSGIPathInfoDispatcher as PathInfoDispatcher
from requestlogger import WSGILogger, ApacheFormatter
from logging import StreamHandler

import threading
import os


from systray_icon import start_systray
from anistream import App


handlers = [StreamHandler(), ]
app = WSGILogger(App(), handlers, ApacheFormatter())

d = PathInfoDispatcher({'/': app})
server = WSGIServer(('0.0.0.0', 80), d)

if __name__ == '__main__':
    start_systray()

    import time
    def start_web():
        while True:
            os.system('plink -load KEEPALIVE -R anistream:80:localhost:80 anistream@serveo.net')
            time.sleep(1)
    threading.Thread(target=start_web, daemon=True).start()

    try:
        server.start()
    except KeyboardInterrupt:
        server.stop()
