import subprocess
import threading
import hashlib
import os

from PIL import Image
import magic

from .config import *


class Thumbnailer:
    def __init__(self):
        self._thumbnailing = []

    def make_thumb(self, original, t_path):
        self._thumbnailing.append(original)

        try:
            if original.endswith(('.mp4', '.mkv')) or magic.from_file(original, mime=True).startswith('video/'):
                sp = subprocess.Popen(f'ffmpeg -i "{original}"', stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
                sp.wait()
                f = sp.stderr.read().decode()

                for i in f.split('\n'):
                    i = i.strip()
                    if i.startswith('Duration'):
                        dur = i.split(':', 1)[1].split(',')[0]
                        break
                else:
                    dur = '00:00:00.01'

                a, b, c = map(float, dur.split(':'))
                length = a * 3600 + b * 60 + c
                pos = length // 5  # 1/5th of the way in
                os.system(f'ffmpeg -hide_banner -loglevel panic -y -an -ss {pos} -i "{original}" -vframes 1 "{t_path}.temp.png"')

                im = Image.open(t_path + '.temp.png')
                im.thumbnail(THUMB_SIZE, Image.ANTIALIAS)
                im.save(t_path, 'JPEG', quality=90)
                os.remove(t_path + '.temp.png')
            elif magic.from_file(original, mime=True).startswith('image/'):
                im = Image.open(original)
                im.thumbnail(THUMB_SIZE, Image.ANTIALIAS)
                im.save(t_path, 'JPEG', quality=90)
        finally:
            self._thumbnailing.remove(original)
        print(f'==> Thumbnailed {original} to {t_path}')

    def get_thumbnail(self, episode):
        path = episode.path

        thumb_name = hashlib.sha1(path.encode()).hexdigest() + '.jpg'
        thumb = os.path.join('static', 'thumbs', thumb_name)

        if not os.path.exists(thumb):
            if path not in self._thumbnailing:
                threading.Thread(target=self.make_thumb, args=(path, thumb)).start()

        return f'/thumbs/{thumb_name}'
