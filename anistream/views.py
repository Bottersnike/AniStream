from flask import send_from_directory


def favicon():
    return send_from_directory('../static/', 'favicon.ico')

def style():
    return send_from_directory('../static', 'index.css')

def manifest():
    return send_from_directory('../static/', 'manifest.json')

def img(path):
    return send_from_directory('../static/img', path)

def thumbs(path):
    return send_from_directory('../static/thumbs', path)