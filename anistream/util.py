from dataclasses import dataclass
from typing import Tuple
import threading
import time
import os
import re

import magic

from .config import *


class Episode:
    def __init__(self, path, thumbs):
        self.path = path
        self.show = path.split(os.path.sep)[-2]
        self._thumbs = thumbs

        self.name = self._clean_filename()
        self.key, self.number = self._extract_episode_number()

        self.ep_str = '{} {}'.format(self.number[0].upper(), self.number[1]) if self.number[0] else str(self.number[1])

    def _clean_filename(self):
        path = os.path.basename(self.path).strip('/')
        name = path.split('/')[-1][::-1].split('.', 1)[-1][::-1].strip()
        name = name.replace('_', ' ').strip()
        while name.startswith('['):
            name = name.split(']', 1)[-1].strip()
        while name.endswith(']'):
            name = name.split('[', 1)[0].strip()

        while name.startswith('('):
            name = name.split(')', 1)[-1].strip()
        while name.endswith(')'):
            name = name.split('(', 1)[0].strip()

        if '[' in name:
            name = name.split('[', 1)[0].strip()
        if name.endswith(')') and ('(1920' in name or '(1280' in name):
            name = name[::-1].split('(', 1)[-1][::-1].strip()

        return name

    def _extract_episode_number(self):
        matches = re.findall(r'-\W*?(\d?\d?\d\.?\d?).*?$', self.name)
        if matches:
            try:
                return int(matches[0]), ('ep', int(matches[0]))
            except ValueError:
                return float(matches[0]), ('ep', float(matches[0]))

        # Make sure OPs are first and EDs last
        typ = ([i for i in ('ed', 'op', 'sp') if i in self.name.lower()] or ['ep'])[0]
        offset = 500 if typ == 'ed' else -500 if typ == 'op' else 0
        matches = re.findall(r'(\d?\d?\d.?\d?)', self.name)
        if matches:
            try:
                return int(matches[0]), (typ, int(matches[0]))
            except ValueError:
                return float(matches[0]), (typ, float(matches[0]))

        return offset, (typ, 1)

    def __str__(self):
        return f'{self.show}/{self.name}'

    def __repr__(self):
        return f'<"{self}" as {hex(id(self)).upper()}>'

    @property
    def video_url(self):
        return f'/v/{self.show}/{self.number[0].lower()}{self.number[1]}'.replace(' ', '_')

    @property
    def url_for(self):
        return f'/{self.show}/{self.number[0].lower()}{self.number[1]}'

    @property
    def href(self):
        return self.url_for.replace(' ', '_')

    @property
    def thumb(self):
        return self._thumbs.get_thumbnail(self)


class Show:
    def __init__(self, path, thumbs):
        self.path = path
        self.name = os.path.basename(path)
        self._episodes = {}
        self._thumbs = thumbs

    @property
    def hidden(self):
        return not os.path.exists(os.path.join(self.path, '.icon.jpg'))

    @property
    def episodes(self):
        episodes = []

        for i in list(self._episodes.keys()):
            if i not in os.listdir(self.path):
                del self._episodes[i]

        for i in os.listdir(self.path):
            i = os.path.join(self.path, i)
            if not is_video(i):
                continue
            if i not in self._episodes:
                self._episodes[i] = Episode(i, self._thumbs)

            episodes.append(self._episodes[i])

        episodes.sort(key=lambda x: x.key)
        return episodes

    @property
    def is_new(self):
        now = time.time()
        for i in os.listdir(self.path):
            if now - os.path.getmtime(os.path.join(self.path, i)) < NEW_THRESH:
                return True
        return False

    @property
    def url(self):
        return '/' + self.name.replace(' ', '_')

    @property
    def icon_url(self):
        return self.url + '/.icon.jpg'

    def suggest(self, episode):
        idx = 0
        # Query the prop once to avoid race conditions
        episodes = self.episodes

        for n, i in enumerate(episodes):
            if i.name == episode.name:
                idx = n
                break
        else:
            return ()

        suggestions = []
        if idx > 0:
            suggestions.append(episodes[idx - 1])
        if idx < len(episodes) - 1:
            suggestions.append(episodes[idx + 1])
        return tuple(suggestions)

    def get_episode(self, episode):
        for i in self.episodes:
            if episode.lower() == f'{i.number[0]}{i.number[1]}'.lower():
                return i
        return None

    def __str__(self):
        s = f'{self.name}/\n'
        for i in self.episodes:
            s += f'    {i.href} | {i.name}\n'
        return s


class ShowManager:
    def __init__(self, thumbs):
        self._shows = {}
        self._lock = threading.Lock()
        self._thumbs = thumbs

    def get_show(self, name):
        # Basic directory traversal prevention
        name = name.replace('_', ' ').replace('..', '')
        name = name.split('/')[-1].split('\\')[-1]

        with self._lock:
            path = os.path.join(LISTING_DIR, name)
            if not os.path.exists(path):
                if path in self._shows:
                    del self._shows[path]
                return None

            if name not in self._shows:
                self._shows[name] = Show(path, self._thumbs)
            show = self._shows[name]
            if show.hidden:
                return None

        return show

    def list_shows(self):
        shows = []
        with self._lock:
            listing = os.listdir(LISTING_DIR)

            for i in list(self._shows.keys()):
                if i not in listing:
                    del self._shows[i]

            for i in listing:
                path = os.path.join(LISTING_DIR, i)
                if i not in self._shows:
                    self._shows[i] = Show(path, self._thumbs)

                if not self._shows[i].hidden:
                    shows.append(self._shows[i])
        return shows

    def locate(self, show, episode):
        show = self.get_show(show)
        if show is None:
            return None, None
        return show, show.get_episode(episode)


def is_video(path):
    return path.endswith('.mp4') or 'video/' in magic.from_file(path, mime=True)
