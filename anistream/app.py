import os

from flask import Flask, send_from_directory, render_template, request, abort

from . import thumbs, views, util


class App(Flask):

    def __init__(self):
        super().__init__(__name__)

        self.thumbs = thumbs.Thumbnailer()
        self.shows = util.ShowManager(self.thumbs)

        self.route('/favicon.ico')(views.favicon)
        self.route('/index.css')(views.style)
        self.route('/manifest.json')(views.manifest)
        self.route('/thumbs/<path:path>')(views.thumbs)
        self.route('/img/<path:path>')(views.img)

        self.route('/<show>')(self.episode_list)
        self.route('/<show>/<episode>')(self.video_page)
        self.route('/v/<show>/<episode>')(self.video_file)

        self.route('/')(self.index)

    # route('/')
    def index(self):
        # Old and new shows are split to bump new shows
        new, old = [], []
        for i in self.shows.list_shows():
            if i.is_new:
                new.append(i)
            else:
                old.append(i)

        listing = sorted(new, key=lambda x: x.name)
        listing += sorted(old, key=lambda x: x.name)

        return render_template('index.html', cards=listing)

    # route('/<show>')
    def episode_list(self, show):
        show = self.shows.get_show(show)
        if show is None:
            return abort(404)

        return render_template('show.html', show=show)

    # route('/<show>/<episode>')
    def video_page(self, show, episode):
        if episode == '.icon.jpg':
            show = self.shows.get_show(show)
            if show is None:
                return abort(404)
            return send_from_directory(show.path, '.icon.jpg')

        show, episode = self.shows.locate(show, episode)
        if episode is None:
            return abort(404)

        suggestions = show.suggest(episode)
        return render_template('watch.html',
                               episode=episode,
                               suggestions=suggestions,
                               show=show)

    # route('/v/<show>/<episode>')
    def video_file(self, show, episode):
        _, episode = self.shows.locate(show, episode)
        if episode is None:
            return abort(404)
        return send_from_directory(os.path.dirname(episode.path), os.path.basename(episode.path))
